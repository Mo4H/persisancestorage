﻿using System;
using System.Xml.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MongoAssignment {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Connect...");

            MongoServer mongo = MongoServer.Create();
            mongo.Connect();

            Console.WriteLine("Connected"); Console.WriteLine();

            var db = mongo.GetDatabase("Assignment3");
            db.DropCollection("users");
            db.DropCollection("lives");
            db.DropCollection("orders");

            using (mongo.RequestStart(db)) {
                // Creating Tables
                var usersTable = db.GetCollection<BsonDocument>("users");
                var livesTable = db.GetCollection<BsonDocument>("lives");
                var ordersTable = db.GetCollection<BsonDocument>("orders");

                // Adding users
                BsonDocument user = new BsonDocument()
                    .Add("persons_name"     , "Roy Springer")
                    .Add("persons_email"    , "roy@royspringer.nl");
                usersTable.Insert(user);

                BsonDocument user2 = new BsonDocument()
                    .Add("persons_name"     , "Gerard Gerardson")
                    .Add("persons_email"    , "Gerard@Gerardson.nl");
                usersTable.Insert(user2);

                BsonDocument user3 = new BsonDocument()
                    .Add("persons_name"     , "Henk Henkins")
                    .Add("persons_email"    , "Henk@Henkins.nl");
                usersTable.Insert(user3);

                var queryHenk = new QueryDocument("persons_name", "Henk Henkins");
                foreach (BsonDocument item in usersTable.Find(queryHenk)) {
                    // Get The user_id
                    BsonElement user_id = item.GetElement("_id");

                    BsonDocument userExtra = new BsonDocument()
                        .Add("user_id"              , user_id.Value)
                        .Add("persons_city"         , "Leiderdorp")
                        .Add("persons_p_code"       , "2354DW")
                        .Add("persons_country"      , "Nederland");
                    livesTable.Insert(userExtra);

                    BsonDocument order = new BsonDocument()
                        .Add("user_id"      , user_id.Value)
                        .Add("order_id"     , BsonValue.Create(ObjectId.GenerateNewId()))
                        .Add("order_nr"     , "405032");
                    ordersTable.Insert(order);
                }

                var queryRoy = new QueryDocument("persons_name", "Roy Springer");
                foreach (BsonDocument item in usersTable.Find(queryRoy)) {
                    // Get The user_id
                    BsonElement user_id = item.GetElement("_id");

                    BsonDocument userExtra = new BsonDocument()
                        .Add("user_id"              , user_id.Value)
                        .Add("persons_city"         , "Amsterdam")
                        .Add("persons_p_code"       , "1023EF")
                        .Add("persons_country"      , "Nederland");
                    livesTable.Insert(userExtra);

                    BsonDocument order = new BsonDocument()
                        .Add("user_id"      , user_id.Value)
                        .Add("order_id"     , BsonValue.Create(ObjectId.GenerateNewId()))
                        .Add("order_nr"     , "32151");
                    ordersTable.Insert(order);

                    BsonDocument order2 = new BsonDocument()
                        .Add("user_id"      , user_id.Value)
                        .Add("order_id"     , BsonValue.Create(ObjectId.GenerateNewId()))
                        .Add("order_nr"     , "231245");
                    ordersTable.Insert(order2);
                }

                var queryGerard = new QueryDocument("persons_name", "Gerard Gerardson");
                foreach (BsonDocument item in usersTable.Find(queryGerard)) {
                    // get the user_id
                    BsonElement user_id = item.GetElement("_id");

                    BsonDocument userExtra = new BsonDocument()
                        .Add("user_id", user_id.Value)
                        .Add("persons_city"     , "Groningen")
                        .Add("persons_p_code"   , "9902GR")
                        .Add("persons_country"  , "Nederland");
                    livesTable.Insert(userExtra);
                }

                // Print data
                foreach (BsonDocument userItem in usersTable.FindAll() ) {
                    BsonElement user_id = userItem.GetElement("_id");
                    BsonElement name    = userItem.GetElement("persons_name");
                    BsonElement email   = userItem.GetElement("persons_email");

                    Console.WriteLine("_id: {0}, Name: {1}, Email: {2}", user_id.Value, name.Value, email.Value);
                    
                    var query = new QueryDocument("user_id", user_id.Value);
                    foreach ( BsonDocument userExtraItem in livesTable.Find( query )) {
                        BsonElement city        = userExtraItem.GetElement("persons_city");
                        BsonElement postalcode  = userExtraItem.GetElement("persons_p_code");
                        BsonElement country     = userExtraItem.GetElement("persons_country");

                        Console.WriteLine("City: {0}, Postalcode: {1}, Country: {2}", city.Value, postalcode.Value, country.Value);
                    }

                    foreach (BsonDocument orderItem in ordersTable.Find(query)) {
                        BsonElement order_id        = orderItem.GetElement("order_id");
                        BsonElement order_number    = orderItem.GetElement("order_nr");

                        Console.WriteLine("Order ID: {0}, Order number: {1}", order_id.Value, order_number.Value);
                    }
                    Console.WriteLine();
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.Read();

            mongo.Disconnect();
        }
    }
}
