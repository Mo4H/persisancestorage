package nl.hva.sqliteapp;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {

	private Button _btnViewLeftJoin, _btnAdd, _btnUpdate, _btnCreatOrder, _btnClearDb;
	private EditText _name, _email, _city, _postalCode, _country, _orderNr;
	private Spinner _namesSpinner1,_namesSpinner2;
	private ArrayAdapter<String> _spinnerAdapter;
	private List<String> _dbItems;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		_btnUpdate = (Button) findViewById(R.id.update);
		_btnAdd = (Button) findViewById(R.id.btnAdd);
		_btnViewLeftJoin = (Button) findViewById(R.id.viewDatabaseLeft);
		_btnCreatOrder = (Button) findViewById(R.id.createOrder);
		_btnClearDb = (Button) findViewById(R.id.clear);
		
		_name = (EditText) findViewById(R.id.name);
		_email = (EditText) findViewById(R.id.email);
		_city = (EditText) findViewById(R.id.city);
		_postalCode = (EditText) findViewById(R.id.postalcode);
		_country = (EditText) findViewById(R.id.country);
		
		_orderNr = (EditText) findViewById(R.id.etOrderNumber);
		_namesSpinner1 = (Spinner) findViewById(R.id.sp1Names);
		_namesSpinner2 = (Spinner) findViewById(R.id.sp2Names);
		_dbItems = new ArrayList<String>();
		
		_spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, _dbItems);
		updateSpinner();
		_namesSpinner1.setAdapter(_spinnerAdapter);
		_namesSpinner2.setAdapter(_spinnerAdapter);
		
		_btnUpdate.setOnClickListener(this);
		_btnAdd.setOnClickListener(this);
		_btnViewLeftJoin.setOnClickListener(this);
		_btnCreatOrder.setOnClickListener(this);
		_btnClearDb.setOnClickListener(this);

	}

	private void updateSpinner(){
		Database entry = new Database(this);
		entry.open();
		_dbItems = entry.getNamesRow();
		_spinnerAdapter.clear();
		for(int i = 0; i < _dbItems.size(); i++){
			_spinnerAdapter.add(_dbItems.get(i));
		}
		entry.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onClick(View arg0) {
		Database entry;
		switch (arg0.getId()) {
		case R.id.btnAdd:
			boolean didItWork = true;
			try {
				String name = _name.getText().toString();
				String email = _email.getText().toString();
				

				entry = new Database(this);
				entry.open();
				entry.addUser(name, email);
				entry.close();
			} catch (Exception e) {
				didItWork = false;
				String error = e.toString();
				Dialog d = new Dialog(this);
				d.setTitle("Failed Saving");
				TextView tv = new TextView(this);
				tv.setText(error);
				d.setContentView(tv);
				d.show();
			} finally {
				if (didItWork) {
					Dialog d = new Dialog(this);
					d.setTitle("Added user");
					TextView tv = new TextView(this);
					tv.setText("Succes!");
					d.setContentView(tv);
					d.show();
				}
				updateSpinner();
			}
			break;
			
		case R.id.update:
			String city = _city.getText().toString();
			String postalCode = _postalCode.getText().toString();
			String country = _country.getText().toString();
			
			String name = (String)_namesSpinner1.getSelectedItem();
			
			entry = new Database(this);
			entry.open();
			int uId = entry.getIdbyName(name);
			boolean succes = true;
			try {
				long errorTest = entry.updateUser(uId, city, postalCode, country);
				Log.d("errorTest", ""+errorTest);
			} catch (Exception e1) {
				succes = false;
			}
			entry.close();
			
			Dialog d = new Dialog(this);
			d.setTitle("Update user");
			TextView tv = new TextView(this);
			if(succes){
				tv.setText("Succes!");
			}else{
				tv.setText("FAILED!!!");
			}
			d.setContentView(tv);
			d.show();
			
			break;
			
		case R.id.viewDatabaseLeft:
			entry = new Database(this);
			entry.open();
			String leftjoin = entry.getLeftJoin();
			entry.close();
			Intent intentLeft = new Intent("nl.hva.sqliteapp.SQLVIEWER");
			intentLeft.putExtra("data", leftjoin );
			startActivity(intentLeft);
			break;
			
		case R.id.clear:
			entry = new Database(this);
			entry.open();
			entry.dropDatabase();
			entry.close();
			updateSpinner();
			break;
			
		case R.id.createOrder:
			String name1 = (String)_namesSpinner2.getSelectedItem();
			String orderText = _orderNr.getText().toString();
			int orderNr = -1;
			try {
				orderNr = Integer.parseInt( orderText );
			} catch (NumberFormatException e) {
				Dialog d1 = new Dialog(this);
				d1.setTitle("Number not correct");
				TextView tv1 = new TextView(this);
				tv1.setText("The number is to big!");
				d1.setContentView(tv1);
				d1.show();
				return;
			}
			entry = new Database(this);
			entry.open();
			
			int userId = entry.getIdbyName(name1);
			boolean orderCreated = true;
			try {
				entry.createOrder(userId, orderNr);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				orderCreated = false;
			}
			entry.close();
			
			Dialog d1 = new Dialog(this);
			d1.setTitle("Order");
			TextView tv2 = new TextView(this);
			if(orderCreated){
				tv2.setText("Succes!");
			}else{
				tv2.setText("FAILED!!!");
			}
			d1.setContentView(tv2);
			d1.show();
			
			break;
		}

	}

}
