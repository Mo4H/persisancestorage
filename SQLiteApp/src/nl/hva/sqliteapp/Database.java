package nl.hva.sqliteapp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Database {
	private static final String DATABASE_NAME = "OrderSystemDB2";
	
	private static final String DATABASE_TABLE_USERS = "users";
	public static final String USER_ID = "user_id";
	public static final String KEY_NAME = "persons_name";
	public static final String KEY_EMAIL = "persons_email";
	
	public static final String DATABASE_TABLE_LIVES = "lives";
	public static final String KEY_CITY = "persons_city";
	public static final String KEY_POSTAL_CODE = "persons_p_code";
	public static final String KEY_COUNTRY = "persons_country";
	
	private static final String DATABASE_TABLE_ORDERS = "orders";
	public static final String ORDER_ID = "order_id";
	public static final String ORDER_NR = "order_nr";
	
	private static final int DATABASE_VERSION = 1;
	
	private final Context _context;
	private SQLiteDatabase _dataBase;
	private DatabaseHelper _dbHelper;
	
	
	public Database(Context c){
		_context = c;
		initDatabase();
	}

	public void initDatabase() {
		_dbHelper = new DatabaseHelper(_context, DATABASE_NAME, DATABASE_VERSION);
		// Creating tables
		_dbHelper.createTabel(DATABASE_TABLE_USERS, new String[]{ 	USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT",
																	KEY_NAME + " TEXT NOT NULL",
																	KEY_EMAIL + " TEXT NOT NULL UNIQUE" } );
		
		_dbHelper.createTabel(DATABASE_TABLE_LIVES, new String[]{ 	USER_ID + " INTEGER",
																	KEY_CITY + " TEXT NOT NULL",
																	KEY_POSTAL_CODE + " TEXT NOT NULL",
																	KEY_COUNTRY + " TEXT NOT NULL"} );
		
		_dbHelper.createTabel(DATABASE_TABLE_ORDERS, new String[]{ 	ORDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT",
																	USER_ID + " INTEGER",
																	ORDER_NR + " INTEGER UNIQUE"} );
		
	}
	
	public Database open() throws SQLException{		
		_dataBase = _dbHelper.getWritableDatabase();
		
		return this;
	}
	
	public void close(){
		_dbHelper.close();
	}

	public long addUser(String name, String email) throws Exception {
		ContentValues cv = new ContentValues();
		cv.put(KEY_NAME, name);
		cv.put(KEY_EMAIL, email);
		long row = _dataBase.insert(DATABASE_TABLE_USERS, null, cv);
		if(row == -1){
			throw new Exception("Insert user failed! User email already exists.");
		}
		return row;
	}
	
	public long updateUser(int user_id, String city, String postalCode, String country) throws Exception {
		ContentValues cv = new ContentValues();
		cv.put(USER_ID, user_id);
		cv.put(KEY_CITY, city);
		cv.put(KEY_POSTAL_CODE, postalCode);
		cv.put(KEY_COUNTRY, country);
		
		String where = USER_ID+"=?";
		String[] whereArgs = new String[] {String.valueOf(user_id)};
		
		long row = _dataBase.update(DATABASE_TABLE_LIVES, cv, where, whereArgs);
		if(row == 0){
			Log.d("Database","User does not exitst instert new data.");
			row = _dataBase.insert(DATABASE_TABLE_LIVES, null, cv);
			if(row == -1){
				throw new Exception("Could not update or insert.");
			}
		}
		return row;
	}
	
	public long createOrder(int user_id, int orderNr) throws Exception {
		ContentValues cv = new ContentValues();
		cv.put(USER_ID, user_id);
		cv.put(ORDER_NR, orderNr);

		long row = _dataBase.insert(DATABASE_TABLE_ORDERS, null, cv);
		if(row == -1){
			throw new Exception("Could not update or insert.");
		}
		return row;
	}

	public String getData() {
		String[] columns = new String[]{ USER_ID, KEY_NAME };
		Cursor c = _dataBase.query(DATABASE_TABLE_USERS, columns, null, null, null, null, null);
		String result = "";
		int iRow = c.getColumnIndex(USER_ID);
		int iName= c.getColumnIndex(KEY_NAME);
		
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result = result + c.getString(iRow) + " " + c.getString(iName)+ "\n";
		}
		
		return result;
	}

	public ArrayList<String> getNamesRow() {
		String[] columns = new String[]{ KEY_NAME };
		Cursor c = _dataBase.query(DATABASE_TABLE_USERS, columns, null, null, null, null, null);
		List<String> result = new ArrayList<String>();
		int iName= c.getColumnIndex(KEY_NAME);
		
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			result.add( c.getString(iName));
		}
		return (ArrayList<String>) result;
	}
	
	public int getIdbyName(String name) {
		String query = "SELECT " + USER_ID + " FROM " + DATABASE_TABLE_USERS + " WHERE " + KEY_NAME + "=?";
		Cursor c = _dataBase.rawQuery(query, new String[]{String.valueOf(name)});
		int indexID = c.getColumnIndex(USER_ID);
		if(c.moveToFirst()){
			return c.getInt(indexID);
		}
		return -1;
		
	}

	public String getLeftJoin() {
		String query = "SELECT * FROM "+ DATABASE_TABLE_USERS +	" LEFT JOIN "+DATABASE_TABLE_LIVES+" ON "+DATABASE_TABLE_USERS+"."+USER_ID+"="+DATABASE_TABLE_LIVES+"."+USER_ID+
																" LEFT JOIN "+DATABASE_TABLE_ORDERS+" ON "+DATABASE_TABLE_USERS+"."+USER_ID+"="+DATABASE_TABLE_ORDERS+"."+USER_ID+";";
		Log.d("Database", ""+query);
		Cursor c = _dataBase.rawQuery(query, null);
		String result = "";
		Log.d("Database", ""+c.getColumnNames());
		
		for(int i = 0; i < c.getColumnNames().length; i++){
			if((i + 1) < c.getColumnNames().length){
				result = result + c.getColumnNames()[i] + " ";
			}else{
				result = result + c.getColumnNames()[i] + "\n";
			}
		}
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			for(int i = 0; i < c.getColumnCount(); i++){
				if((i + 1) < c.getColumnCount()){
					result = result + c.getString(i) + "     ";
				}else{
					result = result + c.getString(i) + "\n";
				}
			}
		}
		return result;
	}
	
	public void dropDatabase(){
		_dbHelper.dropTables(new String[]{ DATABASE_TABLE_USERS, DATABASE_TABLE_ORDERS, DATABASE_TABLE_LIVES } );
	}
}
