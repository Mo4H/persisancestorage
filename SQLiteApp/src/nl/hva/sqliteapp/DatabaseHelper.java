package nl.hva.sqliteapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper{

	private SQLiteDatabase _database;
	private String _databaseName;
	private int _version;
	
	public DatabaseHelper(Context context, String name, int version) {
		super(context, name, null, version);
		_databaseName = name;
		_version = version;
		_database = this.getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d("DatabaseHelper", "onCreate");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		onCreate(db);
	}
	
	public void createTabel(String tableName, String[] columns){
		String execString = "CREATE TABLE IF NOT EXISTS " + tableName.toString() + " (";
		String newLine = ", ";
		String endLine = ");";
		for(int i = 0; i < columns.length; i++){
			if( (i + 1) < columns.length ){
				execString = execString + columns[i].toString() + newLine;
			}else{
				execString = execString + columns[i].toString() + endLine;
			}
		}
		//Log.d("DatabaseHelper", "exec string " + execString + " in database " + _database);
		_database.execSQL( execString );
	}
	
	public void dropTable( String tableName ){
		_database.execSQL("DROP TABLE IF EXISTS "+ tableName);
	}
	
	public void dropTables( String[] tableNames ){
		for(int i = 0; i < tableNames.length; i++){
			dropTable(tableNames[i]);
		}
	}
	
}